CONTENTS OF THIS FILE
---------------------
   
 * Introduction
 * Requirements
 * Recommended modules
 * Installation
 * Configuration
 * Troubleshooting
 * FAQ
 * Maintainers

INTRODUCTION
------------

The prepopulatedfields module allows the creation of curated content and
gives content authors the ability to choose from the previously curated 
content, and use as is, modify, or ignore completely in fields across as
many content types as they are added.

 * For a full description of the module, visit the project page:
   https://drupal.org/project/prepopulatedfields

 * To submit bug reports and feature suggestions, or to track changes:
   https://drupal.org/project/issues/prepopulatedfields

REQUIREMENTS
------------

This module requires the following modules:

 * Entity reference (https://drupal.org/project/entityreference)
 * Taxonomy (https://drupal.org/project/taxonomy)

RECOMMENDED MODULES
-------------------

 * Ckeditor (https://www.drupal.org/project/ckeditor):
   When enabled, allows easy editing and formatting of the content.

INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.

CONFIGURATION
-------------

The module has no menu or modifiable settings. There is no configuration. When
enabled, the module will allow the addition of content of prepopulated_data
content type, and the addition of the prepopulatedfields field type to content
types across your site. Once prepopulated_data has been created, and tagged,
you can filter the content available to content authors by tags on each field
instance.

TROUBLESHOOTING
---------------

 * If the Templates dropdown doesn't display, check the following:

   - Has there been any prepopulated_data content created?

FAQ
---

Will populate this as questions come in on the issue tracker.

MAINTAINERS
-----------

Current maintainers:
 * David Svoboda - https://drupal.org/u/dsvoboda
 * Jason Northrup

This project has been sponsored by:
 * Alt Studios
   Alt Studios is where creative and strategy play nicely together. 
   Made-from-scratch advertising, marketing, PR, events, design and copywriting, 
   targeted to your needs.   
