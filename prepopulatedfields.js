/**
 * @file
 * The prepopulatedfields javascript file.
 */

(function ($) {
  'use strict';
  Drupal.behaviors.prepopulatedfields = {
    attach: function (context, settings) {
      $('[data-prepopulatefields-markup]').each(function (i) {
        var field = $(this).attr('data-field');
        var textarea = $('textarea[name^="' + field + '"]');

        $('select[data-name^="' + field + '"]').on('change', function (e) {
          if (e.currentTarget.selectedIndex > 0) {
            var html = e.currentTarget.value;
            CKEDITOR.instances[textarea.attr('id')].setData(html, {
              callback: function () {
                this.checkDirty();
              }
            });
          }
        });
      });
    }
  };
})(jQuery);
